import threading, time, traceback

class PeriodicBackupThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
    
    def run(self):
        while True:
            try:
                base.startBackup()
                time.sleep(settings['backupInterval'])
            except:
                base.getLogFile().write('%s %s\n' % (time.strftime('[%Y/%m/%d %H:%M:%S]'), traceback.format_exc()))
                base.getLogFile().flush()
                time.sleep(120)
