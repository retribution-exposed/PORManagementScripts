from management.util import Utils
from datetime import datetime

class MegaService(object):

    def __init__(self, username, password, capSpace=-1):
        self.username = username
        self.password = password
        self.capSpace = capSpace
    
    def getUsername(self):
        return self.username
    
    def getPassword(self):
        return self.password
    
    def runProcess(self, process, returnString=True, exceptions=True):
        if isinstance(process, str):
            process = process.split()

        process = process + ['--username', self.username, '--password', self.password]
        response = Utils.runSubprocessToString(process, returnString, True)
        
        if self.isError(response):
            if exceptions:
                raise Exception('Error occurred during MEGA upload.\n' + response)
            else:
                return
        
        return response
    
    def isError(self, response):
        if isinstance(response, str):
            return response.startswith('ERROR:')
        else:
            for resp in response:
                if resp.startswith('ERROR:'):
                    return True

            return False

    def getFreeSpace(self):
        freeSpace = long(self.runProcess(['megadf', '--free']))

        if self.capSpace != -1:
            freeSpace = min(freeSpace, self.capSpace)

        return freeSpace

    def listFiles(self, path, downloadLinks=False):
        options = '-l'

        if downloadLinks:
            options += 'e'

        response = self.runProcess(['megals', options, path], False)
        files = []
        
        for resp in response:
            resp = resp.split()
            
            if '-' in resp:
                continue

            file = {}

            if downloadLinks:
                link = resp[0]
                
                if link.startswith('http'):
                    del resp[0]
                    file['link'] = link
            
            for i, detail in enumerate(['handle', 'owner', 't', 'size', 'date', 'time', 'filename']):
                file[detail] = resp[i]
            
            file['size'] = long(file['size'])
            file['timestamp'] = Utils.getUnixTimestamp(datetime.strptime(file['date'] + ' ' + file['time'], '%Y-%m-%d %H:%M:%S'))
            files.append(file)
        
        return files
    
    def mkdir(self, path, exceptions=True):
        return self.runProcess(['megamkdir', path], exceptions=exceptions)
    
    def removeFile(self, path):
        return self.runProcess(['megarm', path])
    
    def uploadFile(self, remoteFolder, localPath):
        return self.runProcess(['megaput', '--path', remoteFolder, localPath])
