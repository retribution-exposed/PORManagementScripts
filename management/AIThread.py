from management.util import Utils
import subprocess, threading, time, sys, os

class AIThread(threading.Thread):

    def __init__(self, server):
        threading.Thread.__init__(self)
        self.server = server
        self.daemon = True
        self.running = True
        self.process = None
    
    def stop(self):
        self.running = False
        
        if self.process:
            self.server.info("Stopping...")
            self.process.kill()
            self.process = None
    
    def getProcess(self):
        return self.process
    
    def hasProcess(self):
        return self.process is not None
    
    def run(self):
        self.server.emptyLog()
        
        while self.running:
            os.chdir(base.getDirectoryPath('server'))
            self.server.info("Starting...")
            self.server.cycleLogFile()
            path = self.server.getStartPath()
            
            if sys.platform != 'win32':
                subprocess.Popen(['chmod', '+x', path[0]], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                time.sleep(0.1)

            self.process = subprocess.Popen(path, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            
            while self.running:
                line = self.process.stdout.readline()
            
                if not self.process:
                    break
                if line == '' and self.process.poll() is not None:
                    break
                
                if line:
                    self.server.appendLog(line)

            time.sleep(1)
