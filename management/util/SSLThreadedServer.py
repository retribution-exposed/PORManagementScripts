from SocketServer import ThreadingMixIn
from SSLServer import SSLServer

class SSLThreadedServer(ThreadingMixIn, SSLServer):
    daemon_threads = True