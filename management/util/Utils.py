from contextlib import contextmanager
from datetime import datetime
import logging, subprocess, os

epoch = datetime.utcfromtimestamp(0)

def getUnixTimestamp(dt):
    return long((dt - epoch).total_seconds() * 1000.0)

@contextmanager
def closing(thing):
    try:
        yield thing
    finally:
        thing.close()

def getPort(ip):
    return ip.split(':')[-1]

def runSubprocess(process):
    if isinstance(process, str):
        process = process.split(' ')

    base.appendSubprocessLog('[*] Running %s\n' % process)
    
    try:
        process = subprocess.Popen(process, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except:
        return -1
    
    while True:
        line = process.stdout.readline()
            
        if line == '' and process.poll() is not None:
            break
        
        if line:
            base.appendSubprocessLog(line)

    base.appendSubprocessLog('\n')
    return process.returncode == 0

def runSubprocessToString(process, returnString=True, appendSubprocess=False):
    if isinstance(process, str):
        process = process.split()

    if appendSubprocess:
        base.appendSubprocessLog('[*] Running %s\n' % process)

    try:
        process = subprocess.Popen(process, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except:
        return ('' if returnString else [])

    lines = []
    
    while True:
        line = process.stdout.readline()
            
        if line == '' and process.poll() is not None:
            break
        
        if line:
            lines.append(line)
            
            if appendSubprocess:
                base.appendSubprocessLog(line)

    if returnString:
        lines = '\n'.join(lines)

    return lines