from management.util import Utils
import subprocess, logging, threading, time, os

class DeployThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
    
    def run(self):
        logging.info('Starting deployment...')
        base.updateRepositories()
        
        directory = os.path.join(base.getDirectoryPath('build'), 'built')
        git = ['git', '-C', directory]
        
        logging.info('Compiling game...')
        os.chdir(base.getDirectoryPath('build'))
        Utils.runSubprocess(git + ['reset', '--hard'])
        Utils.runSubprocess(['python', '-OO', 'make.py', '-nm'])
        os.chdir(base.getDirectory())

        logging.info("Uploading to release repository...")
        Utils.runSubprocess(git + ['add', '-A'])
        Utils.runSubprocess(git + ['commit', '-m', 'Deployment'])
        Utils.runSubprocess(git + ['push'])
        
        logging.info("Deployment done.")
        base.deployThread = None
