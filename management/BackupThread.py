from management.util import Utils
from ManagerGlobals import *
import logging, threading, shutil, time, os

class BackupThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
    
    def run(self):
        # First, make the backup folder.
        directory = os.path.join(base.getDirectory(), 'backups')
        
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        # Change to the backup folder.
        os.chdir(directory)
        files = []
        
        # Dump all MySQL databases.
        if 'mysqlUsername' in settings and 'mysqlPassword' in settings:
            mysqlOutput = Utils.runSubprocessToString(['mysqldump', '-h', settings.get('mysqlIp', '127.0.0.1'), '-u', settings['mysqlUsername'], '-p%s' % settings['mysqlPassword'], '--all-databases'])
            
            if mysqlOutput:
                with open('mysql_backup.sql', 'w') as f:
                    f.write(mysqlOutput)
                
                files.append('mysql_backup.sql')
        
        # Dump all MongoDB databases.
        if Utils.runSubprocess(['mongodump']):
            files.append('dump')

        # Dump configuration files.
        for file in ['settings.json', 'servers.json']:
            with open(file, 'w') as f1:
                with open(os.path.join(base.getDirectory(), file), 'r') as f2:
                    f1.write(f2.read())
                    files.append(file)

        # Create archive.
        filename = time.strftime('%Y%m%d-%H%M%S') + '.zip'
        remoteFolder = '/Root/Backups'
        Utils.runSubprocess(['zip', '-r', filename] + files)
        
        # Check Mega free space. Space requirement is multiplied to be super sure.
        freeSpace = base.mega.getFreeSpace()
        spaceReq = long(os.path.getsize(filename) * 2.5)
        
        # Try to create the remote folder. If it already exists, it will throw an exception. No problem.
        base.mega.mkdir(remoteFolder, exceptions=False)
        
        if spaceReq >= freeSpace:
            # Shit. We need more space!
            # Let's delete older files until we reach our requirement.
            files = base.mega.listFiles(remoteFolder)
            files = sorted(files, key=lambda f: f['timestamp'])
            
            while spaceReq >= freeSpace and len(files) > 0:
                file = files.pop(0)
                base.mega.removeFile(file['filename'])
                
                freeSpace += file['size']

        # Okay. Let's upload the file.
        base.mega.uploadFile(remoteFolder, filename)
        
        # Let's move out and remove the backup folder.
        os.chdir(base.getDirectory())
        shutil.rmtree(directory)
        
        base.backupThread = None
