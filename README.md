Pirates Online Retribution Management Server
============================================
This repository is the source code for the Pirates Online Retribution Management Server.

### Info

* *Never* upload archives of any kind.
* *Never* force a git push.
* *Always* adhere to the commit naming pattern.